package com.tinytongtong.androidstudy.hook

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivityProxy
import com.tinytongtong.androidstudy.R

class HookTestActivity : AppCompatActivityProxy() {

    companion object {
        fun actionStart(context: Activity) {
            val TAG = HookTestActivity::class.simpleName

            val starter = Intent(context, HookTestActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hook_test)
    }
}