package com.tinytongtong.androidstudy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.ButterKnife;

public class ButterKnifeActivity extends AppCompatActivity {
    public static void actionStart(Context context) {
        Intent starter = new Intent(context, ButterKnifeActivity.class);
        context.startActivity(starter);
    }

    @BindView(R.id.btn_onClick)
    Button btnOnClick;
    @BindView(R.id.btn_onLongClick)
    Button btnLongOnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butter_knife);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_onClick)
    void submit() {
        Toast.makeText(this, "单击事件", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_onLongClick)
    void submit1() {
        Toast.makeText(this, "长按事件", Toast.LENGTH_SHORT).show();
    }
}