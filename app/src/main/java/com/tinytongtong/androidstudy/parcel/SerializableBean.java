package com.tinytongtong.androidstudy.parcel;

import java.io.Serializable;

/**
 * @Description: TODO
 * @Author wangjianzhou
 * @Date 4/19/22 10:03 AM
 * @Version TODO
 */
public class SerializableBean implements Serializable {


    private String name;
    private int age;
    private ABC abc;

    private static final class ABC implements Serializable{

    }
}
