package com.tinytongtong.androidstudy.viewpost

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.tinytongtong.androidstudy.R
import com.tinytongtong.androidstudy.popupwindow.PopupWindowTestActivity
import kotlinx.android.synthetic.main.activity_view_post.*

class ViewPostActivity : AppCompatActivity() {

    companion object {
        private val TAG = ViewPostActivity::class.java.simpleName
        fun actionStart(context: Activity) {
            val starter = Intent(context, ViewPostActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_post)

        tv.post({
            Log.e(
                TAG,
                "measuredWidth:" + tv.measuredWidth + ", measuredHeight:" + tv.measuredHeight
            )
        })
    }

    override fun onResume() {
        super.onResume()
        tv.post({
            Log.e(
                TAG,
                "onResume measuredWidth:" + tv.measuredWidth + ", measuredHeight:" + tv.measuredHeight
            )
        })
    }
}