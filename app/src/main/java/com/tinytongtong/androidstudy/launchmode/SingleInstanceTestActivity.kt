package com.tinytongtong.androidstudy.launchmode

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tinytongtong.androidstudy.R
import com.tinytongtong.androidstudy.rxjava.RxJavaActivity
import kotlinx.android.synthetic.main.activity_single_instance_test.*

class SingleInstanceTestActivity : AppCompatActivity() {

    companion object {
        private val TAG = SingleInstanceTestActivity::class.simpleName
        fun actionStart(context: Activity) {
            val starter = Intent(context, SingleInstanceTestActivity::class.java)
            context.startActivity(starter)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_instance_test)

        btn_launch.setOnClickListener {
            startActivity(Intent(this, RxJavaActivity::class.java))
        }
    }
}