package com.tinytongtong.androidstudy.launchmode

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tinytongtong.androidstudy.R
import kotlinx.android.synthetic.main.activity_launch_mode_ectry.*

class LaunchModeEctryActivity : AppCompatActivity() {
    companion object {
        private val TAG = LaunchModeEctryActivity::class.simpleName
        fun actionStart(context: Activity) {
            val starter = Intent(context, LaunchModeEctryActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch_mode_ectry)

        // SingleTask
        btn_single_task.setOnClickListener {
            startActivity(Intent(this, SingleTaskTestActivity::class.java))
        }

        btn_single_instance.setOnClickListener {
            SingleInstanceTestActivity.actionStart(this)
        }
    }
}