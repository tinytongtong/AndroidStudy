package com.tinytongtong.androidstudy.java.producerconsumer;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @Description: TODO
 * @Author wangjianzhou
 * @Date 4/17/22 9:44 AM
 * @Version TODO
 */
public class ProducerConsumerTest1 {
    public static void main(String[] args) {
        LinkedList<String> queue = new LinkedList<>();
        int maxCapacity = 10;
        for (int i = 0; i < 5; i++) {
            new Thread(new Producer(queue, maxCapacity)).start();
            new Thread(new Consumer(queue, maxCapacity)).start();
        }
    }


    private static final class Producer implements Runnable {
        private LinkedList<String> queue;
        private int maxCapacity;

        public Producer(LinkedList<String> queue, int maxCapacity) {
            this.queue = queue;
            this.maxCapacity = maxCapacity;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                synchronized (queue) {
                    while (queue.size() == maxCapacity) {
                        try {
                            System.out.println("生产者" + Thread.currentThread().getName() + "开始等待...");
                            queue.wait();
                            System.out.println("生产者" + Thread.currentThread().getName() + "被唤醒");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("生产者" + Thread.currentThread().getName() + "，总数:" + queue.size());
                    queue.addLast("生产者 产品的线程：" + Thread.currentThread().getName() + "，第几个：" + i);
                    queue.notifyAll();
                }
            }
        }
    }

    private static final class Consumer implements Runnable {
        private LinkedList<String> queue;
        private int maxCapacity;

        public Consumer(LinkedList<String> queue, int maxCapacity) {
            this.queue = queue;
            this.maxCapacity = maxCapacity;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            System.out.println("消费者" + Thread.currentThread().getName() + "开始等待...");
                            queue.wait();
                            System.out.println("消费者" + Thread.currentThread().getName() + "被唤醒");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    String s = queue.pollLast();
                    System.out.println("消费者" + Thread.currentThread().getName() + "，消费了:" + s);
                    System.out.println("消费者" + Thread.currentThread().getName() + "，总数:" + queue.size());
                    queue.notifyAll();
                }
            }
        }
    }
}
