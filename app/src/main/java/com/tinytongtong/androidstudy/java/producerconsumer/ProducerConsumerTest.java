package com.tinytongtong.androidstudy.java.producerconsumer;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;

/**
 * @Description: 生产者消费者
 * https://segmentfault.com/a/1190000024444906
 * https://www.cnblogs.com/zhengzhaoxiang/p/13976949.html
 * @Author wangjianzhou
 * @Date 4/17/22 8:50 AM
 * @Version TODO
 */
public class ProducerConsumerTest {

    public static void main(String[] args) {
        Queue<Product> queue = new ArrayDeque<>();// offer poll
        int maxCapacity = 10;
        for (int i = 0; i < 10; i++) {
            new Thread(new Producer(queue, maxCapacity)).start();
            new Thread(new Consumer(queue, maxCapacity)).start();
        }
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        for (int i = 0; i < 10; i++) {
//            new Thread(new Consumer(queue, maxCapacity)).start();
//        }
    }

    private static final class Producer implements Runnable {
        private Queue<Product> queue;
        private int maxCapacity;

        public Producer(Queue<Product> queue, int maxCapacity) {
            this.queue = queue;
            this.maxCapacity = maxCapacity;
        }

        @Override
        public void run() {
            synchronized (queue) {
                while (queue.size() == maxCapacity) {
                    try {
                        System.out.println("生产者" + Thread.currentThread().getName() + "等待着=中... Queue 已达最大容量，无法生产");
                        queue.wait();
                        System.out.println("生产者" + Thread.currentThread().getName() + "退出等待");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Product product = new Product("产品" + new Random().nextInt());
                queue.offer(product);
                System.out.println("生产者" + Thread.currentThread().getName() + "生产了产品" + product.toString());
                queue.notifyAll(); //生产新产品了，唤醒所有线程
            }
        }
    }

    private static final class Consumer implements Runnable {
        Queue<Product> queue;
        int maxCapacity;

        public Consumer(Queue<Product> queue, int maxCapacity) {
            this.queue = queue;
            this.maxCapacity = maxCapacity;
        }

        @Override
        public void run() {
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        System.out.println("消费者" + Thread.currentThread().getName() + "等待着... Queue 已缺货，无法消费");
                        queue.wait();
                        System.out.println("消费者" + Thread.currentThread().getName() + "退出等待");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Product product = queue.poll();
                System.out.println("消费者" + Thread.currentThread().getName() + "消费了" + product);
                queue.notifyAll(); // 有新产品到来了，唤醒所有线程
            }
        }
    }

    private static final class Product {
        private String name;

        public Product(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Product{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
