package com.tinytongtong.androidstudy.okhttp;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description: TODO
 * @Author wangjianzhou
 * @Date 2022/5/9 11:19 下午
 * @Version TODO
 */
public class DispatcherAsyncListTest {

    public static void main(String[] args) {
        ArrayDeque<AsyncCall> runningAsyncCalls = new ArrayDeque<>();

        AsyncCall asyncCall = new AsyncCall("1");

        for (int i = 0; i < 5; i++) {
            AsyncCall call = new AsyncCall("1");
            call.copy(asyncCall);
            runningAsyncCalls.add(call);
        }

        for (AsyncCall item : runningAsyncCalls) {
            System.out.println(item);
        }

        Iterator<AsyncCall> it = runningAsyncCalls.iterator();
        while (it.hasNext()) {
            AsyncCall s = it.next();
            System.out.println("iterator: " + s);
        }

        System.out.println("modify atomicInteger");
        runningAsyncCalls.getFirst().atomicInteger.incrementAndGet();

        for (AsyncCall item : runningAsyncCalls) {
            System.out.println(item);
        }
    }

    private static final class AsyncCall {
        private String s;
        private AtomicInteger atomicInteger = new AtomicInteger(0);

        public AsyncCall(String s) {
            this.s = s;
        }

        public void copy(AsyncCall asyncCall) {
            atomicInteger = asyncCall.atomicInteger;
        }

        @Override
        public String toString() {
            return "AsyncCall{" +
                    "s='" + s + '\'' +
                    ", atomicInteger=" + atomicInteger +
                    '}';
        }
    }
}
