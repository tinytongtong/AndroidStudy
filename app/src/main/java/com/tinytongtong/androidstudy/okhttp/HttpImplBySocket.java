package com.tinytongtong.androidstudy.okhttp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import javax.net.ssl.SSLSocketFactory;

/**
 * @Description: 使用socket实现http请求
 * @Author wangjianzhou
 * @Date 2022/5/9 5:53 下午
 * @Version TODO
 */
public class HttpImplBySocket {

    public static final String PATH = "http://restapi.amap.com/v3/weather/weatherInfo?city=110101&key=13cb58f5884f9749287abbead9c658f2";

    public static void main(String[] args) {
//        urlParse();

//        socketHttpGet();

//        socketHttpsGet();

        socketHttpPost();
    }

    private static void urlParse() {
        try {
            URL url = new URL(PATH);
            System.out.println("protocol:" + url.getProtocol());
            System.out.println("host:" + url.getHost());
            System.out.println("file:" + url.getFile());
            System.out.println("query:" + url.getQuery());
            System.out.println("port:" + url.getPort());
            System.out.println("defaultPort:" + url.getDefaultPort());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * http: get请求
     */
    private static void socketHttpGet() {
        try {
            // http
            Socket socket = new Socket("restapi.amap.com",80);

            // https: ssl
//            Socket socket = SSLSocketFactory.getDefault().createSocket("www.baidu.com", 443);

            // 给Socket写数据
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            /**
             * GET /v3/weather/weatherInfo?city=110101&key=13cb58f5884f9749287abbead9c658f2 HTTP/1.1
             * Host: restapi.amap.com
             */
            bw.write("GET /v3/weather/weatherInfo?city=110101&key=13cb58f5884f9749287abbead9c658f2 HTTP/1.1\r\n");
            bw.write("Host: restapi.amap.com\r\n");
            // 换行，头部已结束
            bw.write("\r\n");
            bw.flush();

            // 从Socket中读数据，响应
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String readLine = null;
                if ((readLine = br.readLine()) != null) {
                    System.out.println("响应的数据: " + readLine);
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * https: get请求
     */
    private static void socketHttpsGet() {
        try {
            // http
//            Socket socket = new Socket("www.baidu.com",80);

            // https: ssl
            Socket socket = SSLSocketFactory.getDefault().createSocket("www.baidu.com", 443);

            // 给Socket写数据
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            /**
             * GET / HTTP/1.1
             * Host: www.baidu.com
             */
            bw.write("GET / HTTP/1.1\r\n");
            bw.write("Host: www.baidu.com\r\n");
            // 换行，头部已结束
            bw.write("\r\n");
            bw.flush();

            // 从Socket中读数据，响应
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String readLine = null;
                if ((readLine = br.readLine()) != null) {
                    System.out.println("响应的数据: " + readLine);
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * http: post请求
     */
    private static void socketHttpPost() {
        try {
            // http
            Socket socket = new Socket("restapi.amap.com",80);

            // https: ssl
//            Socket socket = SSLSocketFactory.getDefault().createSocket("www.baidu.com", 443);

            // 给Socket写数据
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            /**
             * POST /v3/weather/weatherInfo?city=110101&key=13cb58f5884f9749287abbead9c658f2 HTTP/1.1
             * Content-Length: 48
             * Content-Type: application/x-www-form-urlencoded
             * Host: restapi.amap.com
             * city=110101&key=13cb58f5884f9749287abbead9c658f2
             */
            bw.write("POST /v3/weather/weatherInfo?city=110101&key=13cb58f5884f9749287abbead9c658f2 HTTP/1.1\r\n");
            bw.write("Content-Length: 48\r\n");
            bw.write("Content-Type: application/x-www-form-urlencoded\r\n");
            bw.write("Host: restapi.amap.com\r\n");
            // 换行，头部已结束
            bw.write("\r\n");

            // body
            bw.write("city=110101&key=13cb58f5884f9749287abbead9c658f2\r\n");
            bw.flush();

            // 从Socket中读数据，响应
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String readLine = null;
                if ((readLine = br.readLine()) != null) {
                    System.out.println("响应的数据: " + readLine);
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
