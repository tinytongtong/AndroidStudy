package com.tinytongtong.androidstudy.context

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.tinytongtong.androidstudy.MyApplication
import com.tinytongtong.androidstudy.R
import kotlinx.android.synthetic.main.activity_context_test.*

class ContextTestActivity : AppCompatActivity() {

    companion object {
        fun actionStart(context: Activity) {
            val starter = Intent(context, ContextTestActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_context_test)

       btn_toast.setOnClickListener {
            Toast.makeText(MyApplication.instance, "toast with app context", Toast.LENGTH_SHORT).show()
        }
    }
}