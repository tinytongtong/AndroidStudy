package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.*

/**
 * @Description: TODO
 *
 * @Author wangjianzhou
 * @Date 2022/4/29 9:42 上午
 * @Version TODO
 */
fun main1(args: Array<String>) {
    log("start")
    runBlocking { // 主线程启动的协程 main
        launch {
            repeat(3) {
                delay(100)
                log("launchA - $it")
            }
        }
        launch {
            repeat(3) {
                delay(100)
                log("launchB - $it")
            }
        }
        GlobalScope.launch { // 默认工作线程启动的协程 DefaultDispatcher-worker-1
            repeat(3) {
                delay(120)
                log("GlobalScope - $it")
            }
        }
    }
    log("end")
    /*
    output:
    [main] start
    [main] launchA - 0
    [main] launchB - 0
    [DefaultDispatcher-worker-1] GlobalScope - 0
    [main] launchA - 1
    [main] launchB - 1
    [DefaultDispatcher-worker-1] GlobalScope - 1
    [main] launchA - 2
    [main] launchB - 2
    [main] end
     */
}

fun main(args: Array<String>) {
    log("start")
    GlobalScope.launch(Dispatchers.IO) {
        delay(600)
        log("GlobalScope")
    }
    runBlocking {
        delay(500)
        log("runBlocking")
    }
    Thread.sleep(200)
    log("end")
    /*
    output:
    [main] start
    [main] runBlocking
    [DefaultDispatcher-worker-1] GlobalScope
    [main] end
     */
}

private fun log(msg: Any?) {
    println("[${Thread.currentThread().name}] $msg")
}