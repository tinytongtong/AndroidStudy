package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @Description: GlobalScope 全局作用域
 * https://juejin.cn/post/6908271959381901325
 *
 * @Author wangjianzhou
 * @Date 2022/4/29 12:05 上午
 * @Version TODO
 */

fun main() {
    log("start")
    GlobalScope.launch {
        launch {
            delay(300)
            log("launch A")
        }
        launch {
            delay(400)
            log("launch B")
        }
        log("GlobalScope")
    }
    log("end")
    Thread.sleep(500)// 保证协程执行完毕
    log("sleep 500 finish")

    /*
    output:
    [main] start
    [main] end
    [DefaultDispatcher-worker-1] GlobalScope
    [DefaultDispatcher-worker-2] launch A
    [DefaultDispatcher-worker-2] launch B
    [main] sleep 500 finish
     */
}

private fun log(msg: Any?) {
    println("[${Thread.currentThread().name}] $msg")
}
