package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

/**
 * @Description:
 * async 也是一个作用于 CoroutineScope 的扩展函数，
 * 和 launch 的区别主要就在于：async 可以返回协程的执行结果，而 launch 不行
 * @Author wangjianzhou
 * @Date 2022/4/29 10:18 下午
 * @Version TODO
 */
fun main(args: Array<String>) {
    val time = measureTimeMillis {
        runBlocking {
            val asyncA = async {
                delay(3000)
                1
            }
            val asyncB = async {
                delay(4000)
                2
            }
            log(asyncA.await() + asyncB.await())
        }
    }
    log(time)
    /*
    output:
    [main] 3
    [main] 4078
     */
}

private fun log(msg: Any?) {
    println("[${Thread.currentThread().name}] $msg")
}