package com.tinytongtong.androidstudy.kotlin.invokejava;

import androidx.annotation.NonNull;

/**
 * @Description: TODO
 * @Author wangjianzhou
 * @Date 4/18/22 4:15 PM
 * @Version TODO
 */
public class JavaBeanTest {
    private String s;

    public JavaBeanTest() {
    }

    public JavaBeanTest(String s) {
        this.s = s;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }
}
