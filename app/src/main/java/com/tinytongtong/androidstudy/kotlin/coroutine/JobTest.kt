package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.*
import java.util.concurrent.CancellationException

/**
 * @Description:
 * Job 是协程的句柄。使用 launch 或 async 创建的每个协程都会返回一个 Job 实例，该实例唯一标识协程并管理其生命周期
 *
 * @Author wangjianzhou
 * @Date 2022/4/29 10:10 下午
 * @Version TODO
 */

fun main(args: Array<String>) {
    // 将协程设置为延迟启动
    val job = GlobalScope.launch(start = CoroutineStart.LAZY) {
        for (i in 0..100) {
            delay(100)
            log("GlobalScope.launch for i:$i")
        }
    }
    job.invokeOnCompletion {
        log("invokeOnCompletion: $it")
    }
    log("1. job.isActive: ${job.isActive}")
    log("1. job.isCancelled: ${job.isCancelled}")
    log("1. job.isCompleted: ${job.isCompleted}")

    job.start()

    log("2. job.isActive: ${job.isActive}")
    log("2. job.isCancelled: ${job.isCancelled}")
    log("2. job.isCompleted: ${job.isCompleted}")

    // 休眠400毫秒后再主动取消协程
    Thread.sleep(400)
    job.cancel(CancellationException("test"))

    // 休眠400毫秒防止JVM过快停止导致 invokeOnCompletion 来不及回调
    Thread.sleep(400)

    log("3. job.isActive: ${job.isActive}")
    log("3. job.isCancelled: ${job.isCancelled}")
    log("3. job.isCompleted: ${job.isCompleted}")
}

private fun log(msg: Any?) {
    println("[${Thread.currentThread().name}] $msg")
}