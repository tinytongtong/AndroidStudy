package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * @Description: coroutineScope
 * https://juejin.cn/post/6908271959381901325
 *
 * @Author wangjianzhou
 * @Date 2022/4/29 10:51 上午
 * @Version TODO
 */

fun main(args: Array<String>) {
    log("start")
    runBlocking {
        log("runBlocking start")
        launch {
            delay(100)
            log("Task from runBlocking")
        }
        coroutineScope {
            launch {
                delay(500)
                log("Task from launch")
            }
            delay(50)
            log("Task from coroutineScope")
        }
        log("Coroutine scope is over")
    }
}

private fun log(msg: Any?) {
    println("[${Thread.currentThread().name}] $msg")
}