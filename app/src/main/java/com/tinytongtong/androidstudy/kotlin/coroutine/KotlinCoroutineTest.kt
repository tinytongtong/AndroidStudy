package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @Description: https://juejin.cn/post/6908271959381901325
 *
 * @Author wangjianzhou
 * @Date 2022/4/28 9:54 下午
 * @Version
 */
fun main(args: Array<String>) {
    GlobalScope.launch(context = Dispatchers.IO) {
        delay(1000)
        log("launch")
    }
    log("start")
    Thread.sleep(2000)
    log("end")
}

private fun log(msg:Any?){
    println("[${Thread.currentThread().name}] $msg")
}