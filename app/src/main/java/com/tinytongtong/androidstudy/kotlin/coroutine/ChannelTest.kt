package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * @Description: TODO
 *
 * @Author wangjianzhou
 * @Date 2022/5/1 11:11 上午
 * @Version TODO
 */

fun main(args: Array<String>) {
    runBlocking {
        logX("start")
        // 1、创建管道
        val channel:ReceiveChannel<Int> = produce {
            (1..5).forEach {
                logX("before send $it")
                channel.send(it)
                logX("after send $it")
            }
        }

        channel.consumeEach {
            logX("channel.receive: $it")
        }

        logX("end")
    }
}

private fun logX(msg: Any?) {
    println(
        """
        ==================================
        $msg
        Thread:${Thread.currentThread().name}
        ==================================
    """.trimIndent()
    )
}