package com.tinytongtong.androidstudy.kotlin.bylazy

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tinytongtong.androidstudy.jetpack.viewmodel.Person

/**
 * @Description: TODO
 *
 * @Author wangjianzhou
 * @Date 2022/5/21 10:12 上午
 * @Version TODO
 */
class ByLazyTest {
    companion object {
        private val TAG = ByLazyTest::class.simpleName
    }
    private val users: List<Person> by lazy {
        println("users by lazy start")
        emptyList<Person>().also {
            println("users by lazy also")
        }.apply {
            println("users by lazy apply")
        }
    }

    fun getUsers1(): List<Person> {
        return users
    }

}

fun main(args: Array<String>) {
    val byLazyTest = ByLazyTest()
    println(byLazyTest.getUsers1())
    println(byLazyTest.getUsers1())
    println(byLazyTest.getUsers1())
    println(byLazyTest.getUsers1())
}