package com.tinytongtong.androidstudy.kotlin.coroutine

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * @Description:
 * launch 是一个作用于 CoroutineScope 的扩展函数，用于在不阻塞当前线程的情况下启动一个协程，
 * 并返回对该协程任务的引用，即 Job 对象
 *
 * @Author wangjianzhou
 * @Date 2022/4/29 10:04 下午
 * @Version TODO
 */
fun main(args: Array<String>) = runBlocking {
    val launchA = launch {
        repeat(3){
            delay(100)
            log("launchA - $it")
        }
    }
    val launchB = launch {
        repeat(3){
            delay(100)
            log("launchB - $it")
        }
    }
    /*
    output:
    [main] launchA - 0
    [main] launchB - 0
    [main] launchA - 1
    [main] launchB - 1
    [main] launchA - 2
    [main] launchB - 2
     */
}

private fun log(msg: Any?) {
    println("[${Thread.currentThread().name}] $msg")
}