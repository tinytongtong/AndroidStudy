package com.tinytongtong.androidstudy.qicqhelper

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.julive.wechathelper.ext.isAccessibilityServiceSettingEnabled
import com.example.julive.wechathelper.ext.openAccessSetting
import com.tinytongtong.androidstudy.R
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton

/**
 * @Description: QQ 的 辅助
 *
 * @Author wangjianzhou
 * @Date 2022/7/10 5:21 下午
 * @Version
 */
class QICQHelperActivity : AppCompatActivity() {
    companion object {
        val TAG = QICQHelperActivity::class.simpleName

        fun actionStart(context: Activity) {
            val starter = Intent(context, QICQHelperActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qicqhelper)

        if (!isAccessibilityServiceSettingEnabled(QICQHelperService::class.java.canonicalName)){
            Toast.makeText(this, "无障碍权限未开启，请手动开启", Toast.LENGTH_SHORT).show()
            alert("\n确定: 体验自动化，点完确定后要依次打开：更多已下载的服务->广告剔除助手->开启服务->确定\n\n取消: 自己手动操作", "请求打开无障碍模式") {
                yesButton {
                    Toast.makeText(this@QICQHelperActivity, "yes", Toast.LENGTH_SHORT).show()
                    openAccessSetting()
                }
                noButton {
                    Toast.makeText(this@QICQHelperActivity, "拒绝", Toast.LENGTH_SHORT).show()
                }
            }.show()
        } else {
            Toast.makeText(this, "无障碍权限已开启", Toast.LENGTH_SHORT).show()
        }
    }
}