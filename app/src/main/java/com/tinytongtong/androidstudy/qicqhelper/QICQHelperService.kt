package com.tinytongtong.androidstudy.qicqhelper

import android.accessibilityservice.AccessibilityService
import android.util.Log
import android.view.accessibility.AccessibilityEvent

/**
 * @Description: AccessibilityService
 *
 * @Author wangjianzhou
 * @Date 2022/7/10 10:15 下午
 * @Version
 */
class QICQHelperService : AccessibilityService() {
    companion object {
        val TAG = QICQHelperService::class.simpleName
    }

    override fun onInterrupt() {
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        val pkgName = event?.packageName.toString()
        val eventType = event?.eventType
        val className = event?.className
        Log.e(
            TAG, String.format(
                "onAccessibilityEvent pkgName:%s, eventType:%s, className:%s",
                pkgName, eventType, className
            )
        )
    }
}