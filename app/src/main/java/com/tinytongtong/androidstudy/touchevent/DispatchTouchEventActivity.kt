package com.tinytongtong.androidstudy.touchevent

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.tinytongtong.androidstudy.R
import kotlinx.android.synthetic.main.activity_dispatch_touch_event.*

class DispatchTouchEventActivity : AppCompatActivity() {
    companion object {
//        val TAG = DispatchTouchEventActivity::class.simpleName
        val TAG = "Custom-Activity"

        fun actionStart(context: Activity) {
            val starter = Intent(context, DispatchTouchEventActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispatch_touch_event)

        btn_layout.setOnClickListener {
            Log.e(TAG, "layout container setOnClickListener#onClick")
        }

        btn_a.setOnClickListener {
            Log.e(TAG, "button A setOnClickListener#onClick")
        }

        btn_b.setOnClickListener {
            Log.e(TAG, "button B setOnClickListener#onClick")
        }
    }
}