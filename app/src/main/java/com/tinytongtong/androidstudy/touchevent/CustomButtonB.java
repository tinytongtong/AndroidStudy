package com.tinytongtong.androidstudy.touchevent;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;

import androidx.annotation.Nullable;

/**
 * @Description: TODO
 * @Author wangjianzhou
 * @Date 2022/5/22 10:55 上午
 * @Version TODO
 */
public class CustomButtonB extends Button {
    private static final String TAG = "Custom-Button-B";

    public CustomButtonB(Context context) {
        super(context);
    }

    public CustomButtonB(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomButtonB(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomButtonB(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.e(TAG, "dispatchTouchEvent ev:" + ev.getAction());
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        Log.e(TAG, "onTouchEvent ev:" + ev.getAction());
        return super.onTouchEvent(ev);
    }
}
