package com.tinytongtong.androidstudy.anr

import android.app.Activity
import android.app.ActivityManager
import android.app.ActivityManager.ProcessErrorStateInfo
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tinytongtong.androidstudy.R
import kotlinx.android.synthetic.main.activity_anr_entry.*
import java.util.concurrent.TimeUnit

class ANREntryActivity : AppCompatActivity() {

    companion object {
        fun actionStart(context: Activity) {
            val starter = Intent(context, ANREntryActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anr_entry)

        // mock anr
        btn_mock_anr.setOnClickListener {
            TimeUnit.SECONDS.sleep(15)
        }

        btn_get_anr_list.setOnClickListener {
            var s: ActivityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            var list: List<ProcessErrorStateInfo?>? = s.processesInErrorState
            println(list)
        }
    }
}