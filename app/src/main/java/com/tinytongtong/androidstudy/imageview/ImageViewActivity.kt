package com.tinytongtong.androidstudy.imageview

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tinytongtong.androidstudy.R

class ImageViewActivity : AppCompatActivity() {
    companion object {
        fun actionStart(context: Activity) {
            val starter = Intent(context, ImageViewActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
    }
}