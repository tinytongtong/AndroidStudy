package com.tinytongtong.androidstudy.popupwindow

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.tinytongtong.androidstudy.R
import kotlinx.android.synthetic.main.activity_popup_window_test.*

private val PopupWindowTestActivity.function: (v: View) -> Unit
    get() = {
        val title = TextView(this)
        title.gravity = Gravity.CENTER
        title.setBackgroundColor(Color.BLUE)
        title.setText("猫了个咪")
        val pop = PopupWindow(
            title,
            ViewGroup.LayoutParams.MATCH_PARENT,
            500
        )

//            pop.showAtLocation(btn_show_pop, Gravity.CENTER, 0, 0)
        pop.showAtLocation(btn_show_pop, Gravity.BOTTOM, 100, 100)

    }

class PopupWindowTestActivity : AppCompatActivity() {
    lateinit var pop: PopupWindow

    companion object {
        fun actionStart(context: Activity) {
            val starter = Intent(context, PopupWindowTestActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popup_window_test)

        btn_show_pop.setOnClickListener {
            val title = TextView(this)
            title.gravity = Gravity.CENTER
            title.setBackgroundColor(Color.BLUE)
            title.setText("猫了个咪")
            pop = PopupWindow(
                title,
                ViewGroup.LayoutParams.MATCH_PARENT,
                500
            )

//            pop.showAtLocation(btn_show_pop, Gravity.CENTER, 0, 0)
            pop.showAtLocation(btn_show_pop, Gravity.BOTTOM, 100, 100)

        }

        btn_hide_pop.setOnClickListener {
            pop.dismiss()
        }
    }
}