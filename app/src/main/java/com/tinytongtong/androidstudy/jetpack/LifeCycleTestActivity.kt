package com.tinytongtong.androidstudy.jetpack

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.tinytongtong.androidstudy.R

class LifeCycleTestActivity : AppCompatActivity() {

    companion object {
        private val TAG = LifeCycleTestActivity::class.simpleName
        fun actionStart(context: Activity) {
            val starter = Intent(context, LifeCycleTestActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_life_cycle_test)
        lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                Log.e(TAG, "onCreate event: $event")
            }
        })
    }

    override fun onStart() {
        super.onStart()
        lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                Log.e(TAG, "onStart event: $event")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                Log.e(TAG, "onResume event: $event")
            }
        })
    }

    override fun onPause() {
        super.onPause()
        lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                Log.e(TAG, "onPause event: $event")
            }
        })
    }

    override fun onStop() {
        super.onStop()
        lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                Log.e(TAG, "onStop event: $event")
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                Log.e(TAG, "onDestroy event: $event")
            }
        })
    }
}