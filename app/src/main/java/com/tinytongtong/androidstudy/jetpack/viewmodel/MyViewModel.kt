package com.tinytongtong.androidstudy.jetpack.viewmodel

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * @Description: TODO
 *
 * @Author wangjianzhou
 * @Date 2022/5/21 12:10 上午
 * @Version TODO
 */
class MyViewModel : ViewModel() {
    companion object {
        private val TAG = ViewModelTestActivity::class.simpleName
    }

    private val users: MutableLiveData<List<Person>> by lazy {
        Log.e(TAG, "users by lazy start")
        MutableLiveData<List<Person>>().also {
            Log.e(TAG, "users by lazy also")
            loadUsers()
        }
    }

//    private val users: MutableLiveData<List<Person>> = MutableLiveData<List<Person>>()

    fun getUsers1(): LiveData<List<Person>> {
        return users
    }

    /**
     * https://developer.android.com/topic/libraries/architecture/viewmodel#blogs
     */
    fun loadUsers() {
        // Do an asynchronous operation to fetch users.
        Log.e(TAG, "loadUsers start")
        // 这里必须异步去获取数据。如果同步修改数据，且通过LiveData#setValue来修改数据，则会导致by lazy被多次调用
        val handlerThread = HandlerThread("abc")
        handlerThread.start()
        val handler = Handler(handlerThread.getLooper())
        handler.postDelayed({
            val list = listOf(
                Person("a", 1),
                Person("b", 2),
                Person("c", 3)
            )
            users.postValue(list)
        }, 5000)
    }

    override fun onCleared() {
        super.onCleared()
        Log.e(TAG, "onCleared")
    }
}