package com.tinytongtong.androidstudy.jetpack.viewmodel

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.tinytongtong.androidstudy.R

class ViewModelTestActivity : AppCompatActivity() {
    companion object {
        val TAG = ViewModelTestActivity::class.simpleName

        fun actionStart(context: Activity) {
            val starter = Intent(context, ViewModelTestActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "onCreate 111")
        setContentView(R.layout.activity_view_model_test)

        // ViewModelProviders 已废弃
//        val model: MyViewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)
        // implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'
        val model: MyViewModel = ViewModelProvider(this).get(MyViewModel::class.java)

        model.getUsers1().observe(this, object : Observer<List<Person>> {
            override fun onChanged(list: List<Person>?) {
                Log.e(TAG, "update UI list:${list.toString()}")
            }
        })
    }


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        Log.e(TAG, "onCreate 222")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.e(TAG, "onRestoreInstanceState 111")
    }

    override fun onRestoreInstanceState(
        savedInstanceState: Bundle?,
        persistentState: PersistableBundle?
    ) {
        super.onRestoreInstanceState(savedInstanceState, persistentState)
        Log.e(TAG, "onRestoreInstanceState 222")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.e(TAG, "onSaveInstanceState 111")
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        Log.e(TAG, "onSaveInstanceState 222")
    }

    override fun onRetainCustomNonConfigurationInstance(): Any? {
        Log.e(TAG, "onRetainCustomNonConfigurationInstance")
        return super.onRetainCustomNonConfigurationInstance()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "onDestroy")
    }
}