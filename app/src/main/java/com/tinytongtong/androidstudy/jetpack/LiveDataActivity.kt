package com.tinytongtong.androidstudy.jetpack

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.tinytongtong.androidstudy.databinding.ActivityLiveDataBinding
import kotlinx.coroutines.*

class LiveDataActivity : AppCompatActivity() {

    val liveData: MutableLiveData<String> = MutableLiveData()

    private val mHandler: Handler = Handler(Looper.getMainLooper())

    private var mThreadHandler: Handler? = null

    companion object {
        private val TAG = LiveDataActivity::class.simpleName
        fun actionStart(context: Activity) {
            val starter = Intent(context, LiveDataActivity::class.java)
            context.startActivity(starter)
        }
    }

    private lateinit var viewBinding: ActivityLiveDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityLiveDataBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        Thread(Runnable {
            Looper.prepare()
            Looper.myLooper()?.let {
                mThreadHandler = Handler(it)
            }
            Looper.loop()
        }).start()

        liveData.observe(this, object : Observer<String> {
            override fun onChanged(t: String?) {
                Log.e(TAG, "onChanged t:$t")
            }
        })

//        // 不能在子线程接收LiveData
//        Thread(Runnable { //     java.lang.IllegalStateException: Cannot invoke observe on a background thread
//            liveData.observe(this, object : Observer<String> {
//                override fun onChanged(t: String?) {
//                    Log.e(TAG, "thread:${Thread.currentThread().name}, onChanged t:$t")
//                }
//            })
//        }).start()

        viewBinding.btnPostValue.setOnClickListener {
            // 只会收到b
            liveData.postValue("a")
            liveData.postValue("b")
        }
        viewBinding.btnSetValue.setOnClickListener {
            // a1、b1均会收到
            liveData.setValue("a1")
            liveData.setValue("b1")
        }

        viewBinding.btnSetPostValue.setOnClickListener {
            // a2、b2均会收到
            liveData.setValue("a2")
            liveData.postValue("b2")
        }

        viewBinding.btnPostSetValue.setOnClickListener {
            // b3、a3均会收到。 b3先收到
            liveData.postValue("a3")
            liveData.setValue("b3")
        }

        // setValue在不可见时设置的值，再次可见时只会收到最新的值。
        viewBinding.btnSetValueInvisible.setOnClickListener {
            // 再次可见时，会收到c4
            mHandler.postDelayed(Runnable {
                Log.e(TAG, "postDelayed 5s setValue, thread:${Thread.currentThread().name}")
                liveData.setValue("a4")
                liveData.setValue("b4")
                liveData.setValue("c4")
            }, 5000)
        }
        // postValue在不可见时设置的值，再次可见时只会收到最新的值。
        viewBinding.btnPostValueInvisible.setOnClickListener {
            // 再次可见时，会收到c5
            mThreadHandler?.postDelayed(Runnable {
                Log.e(TAG, "postDelayed 5s postValue, thread:${Thread.currentThread().name}")
                liveData.postValue("a5")
                liveData.postValue("b5")
                liveData.postValue("c5")
            }, 5000)
        }
    }
}