package com.tinytongtong.androidstudy.jetpack

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tinytongtong.androidstudy.databinding.ActivityJetpackBinding
import com.tinytongtong.androidstudy.jetpack.viewmodel.ViewModelTestActivity

class JetPackActivity : AppCompatActivity() {

    companion object {
        fun actionStart(context: Activity) {
            val starter = Intent(context, JetPackActivity::class.java)
            context.startActivity(starter)
        }
    }

    private lateinit var viewBinding: ActivityJetpackBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // viewBinding
        viewBinding = ActivityJetpackBinding.inflate(layoutInflater)
        var view = viewBinding.root
        setContentView(view)

        viewBinding.btnLifecycle.setOnClickListener {
            LifeCycleTestActivity.actionStart(this)
        }
        viewBinding.btnLivedata.setOnClickListener {
            LiveDataActivity.actionStart(this)
        }
        viewBinding.btnDataBinding.setOnClickListener {
            DataBindingActivity.actionStart(this)
        }
        viewBinding.btnViewModel.setOnClickListener {
            ViewModelTestActivity.actionStart(this)
        }
    }
}