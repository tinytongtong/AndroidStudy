package android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * @Description: 检测View绘制流程耗时
 * @Author wangjianzhou
 * @Date 4/25/22 10:45 PM
 * @Version
 */
public class ViewProxy extends View {
    private static final String TAG = ViewProxy.class.getSimpleName();

    private View mProxyedView = null;

    public void setProxyedView(View view) {
        mProxyedView = view;
    }

    public ViewProxy(Context context) {
        super(context);
    }

    public ViewProxy(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewProxy(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ViewProxy(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @SuppressLint("WrongCall")
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        long startTime = System.nanoTime();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        long endTime = System.nanoTime();
        float time = (endTime - startTime) / 10_000 / 100f;
        Log.e(TAG, "view:" + toString() + ", onMeasure 耗时 time:" + time);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        long startTime = System.nanoTime();
        super.onLayout(changed, left, top, right, bottom);
        long endTime = System.nanoTime();
        float time = (endTime - startTime) / 10_000 / 100f;
        Log.e(TAG, "view:" + toString() + ", onLayout 耗时 time:" + time);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        long startTime = System.nanoTime();
        super.onDraw(canvas);
        long endTime = System.nanoTime();
        float time = (endTime - startTime) / 10_000 / 100f;
        Log.e(TAG, "view:" + toString() + ", onDraw 耗时 time:" + time);
    }
}
