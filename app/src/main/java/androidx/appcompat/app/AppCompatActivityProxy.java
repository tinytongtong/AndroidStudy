package androidx.appcompat.app;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * @Description: 给 androidx.appcompat.app.AppCompatActivity 添加代理
 * @Author wangjianzhou
 * @Date 4/25/22 10:19 AM
 * @Version TODO
 */
public class AppCompatActivityProxy extends AppCompatActivity {
    private static final String TAG = AppCompatActivityProxy.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreate before super.onCreate");
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate after super.onCreate");
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy before super.onDestroy");
        super.onDestroy();
        Log.e(TAG, "onDestroy after super.onDestroy");
    }
}
